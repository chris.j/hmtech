#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct
{
    char username[8];
    char password[8];
    void (*informUser)(char isAuth);
} credentials;

void userIsAuthenticated()
{
    puts("User Authenticated");
    puts("Here is the secret key: bitwarden");
}

void isUserAuthenticated(char isAuth)
{
    if (isAuth == 'Y')
        userIsAuthenticated();
    else
        puts("User Not Authenticated");
}

int main(int argc, char **argv)
{
    unsigned char isAuth = 'N';
    credentials.informUser = &isUserAuthenticated;
    puts("Please enter your username: ");
    gets(credentials.username);
    puts("Please enter your password: ");
    gets(credentials.password);

    if (0 == strncmp(credentials.username, "Chris", 8) && strncmp(credentials.password, "Jenkins", 8) == 0)
    {
        isAuth = 'Y';
    }
    printf("isAuth: %c\n", isAuth);
    credentials.informUser(isAuth);
}